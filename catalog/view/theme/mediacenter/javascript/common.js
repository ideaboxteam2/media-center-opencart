var count = 1;

$(document).ready(function () {


    //header cart
    $('#header_cart_items').mouseenter(function () {
        $('#header_cart').fadeIn();
    });

    $('#header_cart').mouseleave(function () {
        $('#header_cart').fadeOut(500);
    });


    $(document).delegate('[data-close-header-tray="yes"]', 'click', function () {
        $('#header_cart').fadeOut(500);
    });

    /*Mobile Menu*/
    $('.mobile-menu .dropdown-menu a.dropdown-toggle').on('click', function (e) {
        if (!$(this).next().hasClass('show')) {
            $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
        }
        var $subMenu = $(this).next(".dropdown-menu");
        $subMenu.toggleClass('show');


        $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
            $('.mobile-menu .dropdown-submenu .show').removeClass("show");
        });


        return false;
    });
    /*Mobile Menu*/


    /*About Us */
    $(function () {
        //get caption top displacement
        var top = $('.parallax_caption').offset().top - parseFloat($('.parallax_caption').css('marginTop').replace(/auto/, 0));
        //get caption top displacement
        var footTop = $('footer').offset().top - parseFloat($('footer').css('marginTop').replace(/auto/, 0));
        console.log("top", top);
        console.log("footTop", footTop);
        var maxY = footTop - $('footer').outerHeight();
        console.log("maxY", maxY);
        $(window).scroll(function (evt) {
            var y = $(this).scrollTop();
            console.log("y: ", y)
            if (y > maxY) {
                $('.parallax_caption').removeClass('parallax_caption2')

            } else {
                $('.parallax_caption').addClass('parallax_caption2');
            }
        });
    });
    /*About Us */

    /*Contact Page*/
    $('[data-area]').click(function () {

        if ($(window).width() < 1199) {
            var $target = $('html,body');
            $target.animate({scrollTop: 0}, 500);
        }
        var city = $(this).data('city');
        var area = $(this).data('area');
        $('[data-map-area = ' + area + ']').show();
        $('[data-map-area]').hide();
        $('[data-map-area = ' + area + ']').show();
    });
    /*Contact Page*/

    $('[data-category-id]').click(function () {
        var cat_id = $(this).data('category-id');
        $('span#u-menu').remove();
        if (cat_id) {
            $(this).parent().append('<span class="u-menu" id="u-menu"></span>');
        }

        $('[data-parent-category = ' + cat_id + ']').show();
        $('[data-parent-category]').hide();
        $('[data-parent-category = ' + cat_id + ']').show();

        $('[data-main-category-id = ' + cat_id + ']').show();
        $('[data-main-category-id]').hide();
        $('[data-main-category-id = ' + cat_id + ']').show();
    });
    $('.navbar .dropdown').on('hidden.bs.dropdown', function () {
        $(this).find('li.dropdown').removeClass('show open');
        $(this).find('ul.dropdown-menu').removeClass('show open');
    });

    /*Search Mob*/
    $('#search_bar').on('click', function () {
        $('.show_search_bar').fadeIn('slow');
    });

    $('#search_mob input[name=\'search\']').parent().find('button').on('click', function () {
        var url = $('base').attr('href') + 'index.php?route=product/search';

        var value = $('header #search_mob input[name=\'search\']').val();

        if (value) {
            url += '&search=' + encodeURIComponent(value);
        }

        location = url;
    });

    $('#search_mob input[name=\'search\']').on('keydown', function (e) {
        if (e.keyCode == 13) {
            $('header #search_mob input[name=\'search\']').parent().find('button').trigger('click');
        }
    });

    /*Infinite Scroll*/
    $("#listproduct").addClass("infinitescroll");
    var $container = $('.infinitescroll');
    $container.infinitescroll({
            navSelector: ".pagination",
            nextSelector: ".next-pagination",
            itemSelector: ".product-layout",
            loading: {
                loadingText: "Loading new products...",
                img: 'image/ajax-loader.gif'
            }
        },
        function () {
            // pause when new elements are loaded
            count++;
            /* if (count == 2) {
             $container.infinitescroll('pause');
             $container.append('<div style="margin: auto;text-align:center;"><button id="btnloadmore" onclick="resume_infinite_scroll()">load more</button></div>');
             }*/
            if (count == $("#maxpage").val()) {
                $("#btnloadmore").hide();
                $container.infinitescroll('destroy');
                //  $container.append('<div style="margin: auto;text-align:center;"><label>Đã hết bài viết</label></div>');
            }
        });
    function resume_infinite_scroll() {
        $("#btnloadmore").hide();
        var $container = $('.infinitescroll');
        $container.infinitescroll('resume');
        $container.infinitescroll('scroll');
    }

    /*Infinite Scroll*/


    /*BreadCrumb Replace*/
    $('.breadcrumb > li > a > i').replaceWith('Home');
    $('<a class="breadcrumb-arrow">></a>').appendTo(".breadcrumb > li");
    /*BreadCrumb Replace*/
    /*Mega Menu*/

    if ($(window).width() >= 980) {

        // when you hover a toggle show its dropdown menu
        $(".navbar .dropdown-toggle").hover(function () {
            $(this).parent().toggleClass("show");
            $(this).parent().find(".dropdown-menu").toggleClass("show");
        });

        // hide the menu when the mouse leaves the dropdown
        $(".navbar .dropdown-menu").mouseleave(function () {
            $(this).removeClass("show");
        });

        // do something here
    }

    /*Mega Menu*/

    $('[data-toggle="slide-collapse"]').on('click', function () {
        $navMenuCont = $($(this).data('target'));
        $navMenuCont.animate({
            'width': 'toggle'
        }, 350);
        $(".menu-overlay").fadeIn(500);

    });
    $("#cross_menu").click(function (event) {
        $(".navbar-toggle").trigger("click");
        $(".menu-overlay").fadeOut(500);
    });
    /*Owl*/
    var product_img_thumbs = $('.product-images-thumb');
    var product_img = $('.product-images');
    product_img.owlCarousel({
        margin: 0,
        loop: false,
        items: 1,
        nav: false,
        dots: false,
        mouseDrag: false,
    });


    product_img_thumbs.owlCarousel({
        margin: 0,
        loop: false,
        items: 4,
        nav: true,
        dots: false,
        responsive: {
            992: {
                items: 4,
                nav: true
            },
            767: {
                items: 2,
                nav: true
            },
            190: {
                items: 2,
                nav: true
            }
        }
    });

    product_img_thumbs.on('click', '.owl-item', function (event) {
        //   alert($(this).index());
        product_img.trigger('to.owl.carousel', [$(this).index(), 300, true]);
    });


    $('.cart-related-slider').owlCarousel({
        margin: 0,
        loop: true,
        items: 1,
        nav: true,
        autoplay: true,
        autoplayTimeout: 1500,
        autoplayHoverPause: true,
    });
    $('.related-products').owlCarousel({
        margin: 0,
        loop: false,
        responsive: {
            992: {
                items: 4,
                nav: true
            },
            767: {
                items: 2,
                nav: true
            },
            190: {
                items: 1,
                nav: true
            }
        }
    });

    $('.products-sale').owlCarousel({
        margin: 0,
        loop: false,
        responsive: {
            992: {
                items: 3,
                nav: true
            },
            767: {
                items: 2,
                nav: true
            },
            190: {
                items: 1,
                nav: true
            }
        }
    });

    /*Owl*/
    if ($(window).width() > 1199) {
        $('.product_zoomer').zoom({
            on: 'click',
            magnify: 0.8,
            onZoomOut: true,
            onZoomIn: true
        });
    }

    product_description();
});

function product_description() {
    var $product_description = $('#product_long_description');
    var $product_description_p = $('#product_long_description p');
    $product_description_p.replaceWith(function () {
        return $('<div/>', {
            html: this.innerHTML
        });
    });

    $product_description.find('img').css('width', '100%')
    var get_paragraph = $product_description.children('div:odd');
    var get_image = $product_description.children('div:even');

    if ($(get_paragraph).not('class')) {
        get_paragraph.addClass('col-lg-6 col-md-6 col-sm-12 justify-content-between align-items-center p-lg-5 p-md-3 p-5 border-bottom text-size-12 goth-bold');
    }

    if ($(get_image).not('class')) {
        if ($(window).width() < 992) {
            $product_description.children('div:even').children('img').css('width', '100%');
        }
        get_image.addClass('col-lg-6 col-md-6 col-sm-12 justify-content-start align-items-end p-0 border-bottom');
    }


    /*Show More*/
    size_li = $('#product_long_description div').size();
    x = 4;
    $('#product_long_description div:lt(' + x + ')').css('display', '-webkit-box');
    $('#loadMore').click(function () {
        x = (x + 5 <= size_li) ? x + 5 : size_li;
        $('#product_long_description div:lt(' + x + ')').css('display', '-webkit-box');
        $('#showLess').css('display', 'block');
        if (x == size_li) {
            $('#loadMore').css('display', 'none');
        }
    });
    $('#showLess').click(function () {
        x = (-1 < 0) ? 4 : x - 5;
        $('#product_long_description div').not(':lt(' + x + ')').css('display', 'none');
        $('#loadMore').css('display', 'block');
        $('#showLess').css('display', 'block');
        if (x == 4) {
            $('#showLess').css('display', 'none');
        }
    });
    /*Show More*/


}