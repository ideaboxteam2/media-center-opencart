<?php

class ControllerExtensionModuleBrandsSlider extends Controller
{
    public function index($setting = null)
    {
        $this->load->language('extension/module/brands_slider');
        $brands = array();
        if ($setting && $setting['status']) {
            /*Calling ALl Brands*/
            $this->load->model('catalog/manufacturer');
            $manufacturers = $this->model_catalog_manufacturer->getManufacturers();
            foreach ($manufacturers as $manufacturer) {
                $brands['categories'][] = array(
                    'name' => $manufacturer['name'],
                    'image' => $manufacturer['image'],
                    'children' => array(),
                    'column' => 1,
                    'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $manufacturer['manufacturer_id'])
                );
            }
            /*Calling ALl Brands*/
            $data = array();
            $data['name'] = sprintf($setting['name']);
            $data['title'] = $setting['title'];
            $data['limit'] = $setting['limit'];
            $data['manufacturer'] = $brands['categories'];
            return $this->load->view('extension/module/brands_slider', $data);
        }
    }
}