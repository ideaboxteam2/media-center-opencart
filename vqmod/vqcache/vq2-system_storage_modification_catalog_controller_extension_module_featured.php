<?php
class ControllerExtensionModuleFeatured extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/featured');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['products'] = array();

		if (!$setting['limit']) {
			$setting['limit'] = 4;
		}

		if (!empty($setting['product'])) {
			$products = array_slice($setting['product'], 0, (int)$setting['limit']);

$customer_wishlist = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_wishlist cw WHERE customer_id = '" . (int)$this->customer->isLogged() . "' AND cw.customer_id = '" . (int)$this->config->get('config_language_id') . "'")->rows;
            $cw = [];

          if($customer_wishlist){
            foreach ($customer_wishlist as $item) {
                $cw[] = array(
                    'customer_id' => $item['customer_id'],
                    'product_id' => $item['product_id'],
                    'date_added' => $item['date_added'],
                );
            }
          }

             $scw = [];
            $sessio_customer_wishlist = isset($this->session->data['wishlist']) ? $this->session->data['wishlist'] : '';
           if($sessio_customer_wishlist){
            foreach ($sessio_customer_wishlist as $item) {
                $scw[] = array(
                    'product_id' => $item,
                );
            }
            }
			foreach ($products as $product_id) {
				$product_info = $this->model_catalog_product->getProduct($product_id);

				if ($product_info) {
					if ($product_info['image']) {
						$image = '/image/'.$product_info['image'];
					} else {
						$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
					}

					if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$price = false;
					}

					if ((float)$product_info['special']) {
						$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$special = false;
					}

					if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
					} else {
						$tax = false;
					}

					if ($this->config->get('config_review_status')) {
						$rating = $product_info['rating'];
					} else {
						$rating = false;
					}


    if(!empty($product_info['short_description']) && ($this->config->get('module_shortdescription_featured')) && ($this->config->get('module_shortdescription_status'))){
      $short_description=strip_tags(html_entity_decode($product_info['short_description'], ENT_QUOTES, 'UTF-8'));
    } else {
      $short_description='';
    }
					$data['products'][] = array(
'manufacturer'       => $product_info['manufacturer'],
                        'manufacturer_link'       => str_replace(' ', '-', $product_info['manufacturer']),
'wishlist_products' => isset($cw) ? $cw : '',
 'session_wishlist_products' => isset($scw) ? $scw : '',
   'customer_available' => isset($this->session->data['customer_id']) ? $this->session->data['customer_id'] : '0',
						'product_id'  => $product_info['product_id'],
						'thumb'       => $image,
						'name'        => $product_info['name'],
						'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',

        'short_description'       => utf8_substr(strip_tags(html_entity_decode($short_description, ENT_QUOTES, 'UTF-8')), 0, $this->config->get('module_shortdescription_limit')) . '..',
        
						'price'       => $price,
						'special'     => $special,
						'tax'         => $tax,
						'rating'      => $rating,
						'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
					);
				}
			}
		}

		if ($data['products']) {
			return $this->load->view('extension/module/featured', $data);
		}
	}
}