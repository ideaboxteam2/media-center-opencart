<?php
// Heading
$_['heading_title'] 		= 'Brands Slider Module';
// Text
$_['text_extension']		= 'Extensions';
$_['text_success']		= 'Success: You have modified banner module!';
$_['text_edit']			= 'Edit Brands Slider Module';
// Entry
$_['entry_name']		= 'Module Name';
$_['entry_title']		= 'Slider Title';
$_['entry_limit']		= 'Slider Limit';
$_['entry_status'] = 'Status';;
// Placeholder
$_['placeholder_name'] 		= 'Module Name';
$_['placeholder_title'] 	= 'You must be %s. Are you?';
$_['placeholder_limit'] 	= 'http://www.example.com';
// Help
$_['help_message'] 		= 'You might use %s expression. It will be replaced with age value.';
// Error
$_['error_name'] 		= 'Module Name';
$_['error_title'] 		= 'Title';
$_['error_limit'] 		= 'Limit';

$_['error_warning'] 		= 'There are problems with data provided';
$_['error_permission'] 		= 'You don\'t have permission to edit this.';

// Button
$_['button_save'] 		= 'Save form';
$_['button_cancel'] 		= 'Cancel';