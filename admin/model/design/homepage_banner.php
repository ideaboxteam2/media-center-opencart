<?php

class ModelDesignHomepageBanner extends Model
{
    public function addHomepageBanner($data)
    {
        $this->db->query("INSERT INTO " . DB_PREFIX . "homepage_banner SET name = '" . $this->db->escape($data['name']) . "', position = '" . $this->db->escape($data['position']) . "', grid_desktop = '" . $this->db->escape($data['grid_desktop']) . "', grid_table = '" . $this->db->escape($data['grid_table']) . "', grid_mobile = '" . $this->db->escape($data['grid_mobile']) . "', status = '" . (int)$data['status'] . "'");

        $homepage_banner_id = $this->db->getLastId();

        if (isset($data['homepage_banner_image'])) {

            foreach ($data['homepage_banner_image'] as $language_id => $value) {

                foreach ($value as $homepage_banner_image) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "homepage_banner_image SET banner_id = '" . (int)$homepage_banner_id . "', language_id = '" . (int)$language_id . "', description = '" . $this->db->escape($homepage_banner_image['description']) . "', image = '" . $this->db->escape($homepage_banner_image['image']) . "', sort_order = '" . (int)$homepage_banner_image['sort_order'] . "',  status = '" . (int)$homepage_banner_image['status'] . "'");
                }
            }
        }

        return $homepage_banner_id;
    }

    public function editHomepageBanner($homepage_banner_id, $data)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "homepage_banner SET name = '" . $this->db->escape($data['name']) . "', position = '" . $this->db->escape($data['position']) . "', grid_desktop = '" . $this->db->escape($data['grid_desktop']) . "', grid_table = '" . $this->db->escape($data['grid_table']) . "', grid_mobile = '" . $this->db->escape($data['grid_mobile']) . "',  status = '" . (int)$data['status'] . "' WHERE banner_id = '" . (int)$homepage_banner_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "homepage_banner_image WHERE banner_id = '" . (int)$homepage_banner_id . "'");

        if (isset($data['homepage_banner_image'])) {
            foreach ($data['homepage_banner_image'] as $language_id => $value) {
                foreach ($value as $homepage_banner_image) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "homepage_banner_image SET banner_id = '" . (int)$homepage_banner_id . "', language_id = '" . (int)$language_id . "', description = '" . $this->db->escape($homepage_banner_image['description']) . "', image = '" . $this->db->escape($homepage_banner_image['image']) . "', sort_order = '" . (int)$homepage_banner_image['sort_order'] . "',  status = '" . (int)$homepage_banner_image['status'] . "'");
                }
            }
        }
    }

    public function deleteHomepageBanner($homepage_banner_id)
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "homepage_banner WHERE banner_id = '" . (int)$homepage_banner_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "homepage_banner_image WHERE banner_id = '" . (int)$homepage_banner_id . "'");
    }

    public function getHomepageBanner($homepage_banner_id)
    {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "homepage_banner WHERE banner_id = '" . (int)$homepage_banner_id . "'");

        return $query->row;
    }

    public function getHomepageBanners($data = array())
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "homepage_banner";

        $sort_data = array(
            'name',
            'status'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getHomepageBannerImage($homepage_banner_id)
    {
        $homepage_banner_image_data = array();

        $homepage_banner_image_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "homepage_banner_image WHERE banner_id = '" . (int)$homepage_banner_id . "' ORDER BY sort_order ASC");

        foreach ($homepage_banner_image_query->rows as $homepage_banner_image) {
            $homepage_banner_image_data[$homepage_banner_image['language_id']][] = array(
                'description' => $homepage_banner_image['description'],
                'image' => $homepage_banner_image['image'],
                'sort_order' => $homepage_banner_image['sort_order'],
                'status' => $homepage_banner_image['status'],
            );
        }
        return $homepage_banner_image_data;
    }

    public function getTotalHomepageBanners()
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "homepage_banner");

        return $query->row['total'];
    }
}
