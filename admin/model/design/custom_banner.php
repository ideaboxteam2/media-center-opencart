<?php

class ModelDesignCustomBanner extends Model
{
    public function addCustomBanner($data)
    {
        $this->db->query("INSERT INTO " . DB_PREFIX . "custom_banner SET name = '" . $this->db->escape($data['name']) . "',  description = '" . $this->db->escape($data['description']) . "', type = '" . $this->db->escape($data['type']) . "', manufacturer = '" . $this->db->escape($data['manufacturer']) . "', category_id = '" . $this->db->escape($data['category_id']) . "', manufacturer_id = '" . $this->db->escape($data['manufacturer_id']) . "',status = '" . (int)$data['status'] . "'");

        $custom_banner_id = $this->db->getLastId();

        if (isset($data['custom_banner_image'])) {

            foreach ($data['custom_banner_image'] as $language_id => $value) {

                foreach ($value as $custom_banner_image) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "custom_banner_image SET banner_id = '" . (int)$custom_banner_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($custom_banner_image['title']) . "', link = '" . $this->db->escape($custom_banner_image['link']) . "', image = '" . $this->db->escape($custom_banner_image['image']) . "', image_m = '" . $this->db->escape($custom_banner_image['image_m']) . "', sort_order = '" . (int)$custom_banner_image['sort_order'] . "'");
                }
            }
        }

        return $custom_banner_id;
    }

    public function editCustomBanner($custom_banner_id, $data)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "custom_banner SET name = '" . $this->db->escape($data['name']) . "', description = '" . $this->db->escape($data['description']) . "', type = '" . $this->db->escape($data['type']) . "', manufacturer = '" . $this->db->escape($data['manufacturer']) . "', category_id = '" . $this->db->escape($data['category_id']) . "', manufacturer_id = '" . $this->db->escape($data['manufacturer_id']) . "', status = '" . (int)$data['status'] . "' WHERE banner_id = '" . (int)$custom_banner_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "custom_banner_image WHERE banner_id = '" . (int)$custom_banner_id . "'");

        if (isset($data['custom_banner_image'])) {
            foreach ($data['custom_banner_image'] as $language_id => $value) {
                foreach ($value as $custom_banner_image) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "custom_banner_image SET banner_id = '" . (int)$custom_banner_id . "', language_id = '" . (int)$language_id . "', title = '" . $this->db->escape($custom_banner_image['title']) . "', link = '" . $this->db->escape($custom_banner_image['link']) . "', image = '" . $this->db->escape($custom_banner_image['image']) . "', image_m = '" . $this->db->escape($custom_banner_image['image_m']) . "', sort_order = '" . (int)$custom_banner_image['sort_order'] . "'");
                }
            }
        }
    }

    public function deleteCustomBanner($custom_banner_id)
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "custom_banner WHERE banner_id = '" . (int)$custom_banner_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "custom_banner_image WHERE banner_id = '" . (int)$custom_banner_id . "'");
    }

    public function getCustomBanner($custom_banner_id)
    {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "custom_banner WHERE banner_id = '" . (int)$custom_banner_id . "'");

        return $query->row;
    }

    public function getCustomBanners($data = array())
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "custom_banner";

        $sort_data = array(
            'name',
            'status'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getCustomBannerImages($custom_banner_id)
    {
        $custom_banner_image_data = array();

        $custom_banner_image_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "custom_banner_image WHERE banner_id = '" . (int)$custom_banner_id . "' ORDER BY sort_order ASC");

        foreach ($custom_banner_image_query->rows as $custom_banner_image) {
            $custom_banner_image_data[$custom_banner_image['language_id']][] = array(
                'title' => $custom_banner_image['title'],
                'link' => $custom_banner_image['link'],
                'image' => $custom_banner_image['image'],
                'image_m' => $custom_banner_image['image_m'],
                'sort_order' => $custom_banner_image['sort_order']
            );
        }
        return $custom_banner_image_data;
    }

    public function getTotalCustomBanners()
    {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "custom_banner");

        return $query->row['total'];
    }
}
