<?php

class ControllerDesignHomepageBanner extends Controller
{
    private $error = array();

    public function index()
    {
        $this->load->language('design/homepage_banner');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('design/homepage_banner');

        $this->getList();
    }

    public function add()
    {
        $this->load->language('design/homepage_banner');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('design/homepage_banner');


        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $this->model_design_homepage_banner->addHomepageBanner($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }
            $this->response->redirect($this->url->link('design/homepage_banner', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getForm();
    }

    public function edit()
    {
        $this->load->language('design/homepage_banner');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('design/homepage_banner');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $this->model_design_homepage_banner->editHomepageBanner($this->request->get['banner_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('design/homepage_banner', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getForm();
    }

    public function delete()
    {
        $this->load->language('design/homepage_banner');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('design/homepage_banner');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $homepage_banner_id) {
                $this->model_design_homepage_banner->deleteHomepageBanner($homepage_banner_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('design/homepage_banner', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getList();
    }

    protected function getList()
    {
        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'name';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('design/homepage_banner', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        $data['add'] = $this->url->link('design/homepage_banner/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['delete'] = $this->url->link('design/homepage_banner/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);

        $data['homepage_banners'] = array();

        $filter_data = array(
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $homepage_banner_total = $this->model_design_homepage_banner->getTotalHomepageBanners();

        $results = $this->model_design_homepage_banner->getHomepageBanners($filter_data);

        foreach ($results as $result) {
            $data['homepage_banners'][] = array(
                'banner_id' => $result['banner_id'],
                'name' => $result['name'],
                'position' => $result['position'],
                'status' => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
                'edit' => $this->url->link('design/homepage_banner/edit', 'user_token=' . $this->session->data['user_token'] . '&banner_id=' . $result['banner_id'] . $url, true)
            );
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array)$this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }


        $url = '';

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['sort_name'] = $this->url->link('design/homepage_banner', 'user_token=' . $this->session->data['user_token'] . '&sort=name' . $url, true);
        $data['sort_status'] = $this->url->link('design/homepage_banner', 'user_token=' . $this->session->data['user_token'] . '&sort=status' . $url, true);


        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total = $homepage_banner_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('design/homepage_banner', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($homepage_banner_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($homepage_banner_total - $this->config->get('config_limit_admin'))) ? $homepage_banner_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $homepage_banner_total, ceil($homepage_banner_total / $this->config->get('config_limit_admin')));

        $data['sort'] = $sort;
        $data['order'] = $order;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('design/homepage_banner_list', $data));
    }

    protected function getForm()
    {
        $data['text_form'] = !isset($this->request->get['banner_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['homepage_banner_image'])) {
            $data['error_homepage_banner_image'] = $this->error['homepage_banner_image'];
        } else {
            $data['error_homepage_banner_image'] = array();
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('design/homepage_banner', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        if (!isset($this->request->get['banner_id'])) {
            $data['action'] = $this->url->link('design/homepage_banner/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        } else {
            $data['action'] = $this->url->link('design/homepage_banner/edit', 'user_token=' . $this->session->data['user_token'] . '&banner_id=' . $this->request->get['banner_id'] . $url, true);
        }

        $data['cancel'] = $this->url->link('design/homepage_banner', 'user_token=' . $this->session->data['user_token'] . $url, true);

        if (isset($this->request->get['banner_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $homepage_banner_info = $this->model_design_homepage_banner->getHomepageBanner($this->request->get['banner_id']);
        }

        $data['user_token'] = $this->session->data['user_token'];

        if (isset($this->request->post['name'])) {
            $data['name'] = $this->request->post['name'];
        } elseif (!empty($homepage_banner_info)) {
            $data['name'] = $homepage_banner_info['name'];
        } else {
            $data['name'] = '';
        }

        if (isset($this->request->post['position'])) {
            $data['position'] = $this->request->post['position'];
        } elseif (!empty($homepage_banner_info)) {
            $data['position'] = $homepage_banner_info['position'];
        } else {
            $data['position'] = '';
        }


        if (isset($this->request->post['grid_desktop'])) {
            $data['grid_desktop'] = $this->request->post['grid_desktop'];
        } elseif (!empty($homepage_banner_info)) {
            $data['grid_desktop'] = $homepage_banner_info['grid_desktop'];
        } else {
            $data['grid_desktop'] = '';
        }

        if (isset($this->request->post['grid_table'])) {
            $data['grid_table'] = $this->request->post['grid_table'];
        } elseif (!empty($homepage_banner_info)) {
            $data['grid_table'] = $homepage_banner_info['grid_table'];
        } else {
            $data['grid_table'] = '';
        }

        if (isset($this->request->post['grid_mobile'])) {
            $data['grid_mobile'] = $this->request->post['grid_mobile'];
        } elseif (!empty($homepage_banner_info)) {
            $data['grid_mobile'] = $homepage_banner_info['grid_mobile'];
        } else {
            $data['grid_mobile'] = '';
        }

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($homepage_banner_info)) {
            $data['status'] = $homepage_banner_info['status'];
        } else {
            $data['status'] = true;
        }


        $this->load->model('localisation/language');

        $data['languages'] = $this->model_localisation_language->getLanguages();

        $this->load->model('tool/image');

        if (isset($this->request->post['homepage_banner_image'])) {
            $homepage_banner_images = $this->request->post['homepage_banner_image'];
        } elseif (isset($this->request->get['banner_id'])) {
            $homepage_banner_images = $this->model_design_homepage_banner->getHomepageBannerImage($this->request->get['banner_id']);
        } else {
            $homepage_banner_images = array();
        }

        $data['homepage_banner_image'] = array();

        foreach ($homepage_banner_images as $key => $value) {
            foreach ($value as $homepage_banner_image) {
                if (is_file(DIR_IMAGE . $homepage_banner_image['image'])) {
                    $image = $homepage_banner_image['image'];
                    $thumb = $homepage_banner_image['image'];
                } else {
                    $image = '';
                    $thumb = 'no_image.png';
                }

                $data['homepage_banner_image'][$key][] = array(
                    'description' => $homepage_banner_image['description'],
                    'image' => $image,
                    'thumb' => $this->model_tool_image->resize($thumb, 100, 100),
                    'sort_order' => $homepage_banner_image['sort_order'],
                    'status' => $homepage_banner_image['status'],
                );
            }
        }

        $data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('design/homepage_banner_form', $data));
    }

    protected function validateForm()
    {
        if (!$this->user->hasPermission('modify', 'design/homepage_banner')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }


        return !$this->error;
    }

    protected function validateDelete()
    {
        if (!$this->user->hasPermission('modify', 'design/homepage_banner')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }
}