<?php

class ControllerDesignCustomBanner extends Controller
{
    private $error = array();

    public function index()
    {
        $this->load->language('design/custom_banner');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('design/custom_banner');

        $this->getList();
    }

    public function add()
    {
        $this->load->language('design/custom_banner');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('design/custom_banner');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_design_custom_banner->addCustomBanner($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('design/custom_banner', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getForm();
    }

    public function edit()
    {
        $this->load->language('design/custom_banner');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('design/custom_banner');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_design_custom_banner->editCustomBanner($this->request->get['banner_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('design/custom_banner', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getForm();
    }

    public function delete()
    {
        $this->load->language('design/custom_banner');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('design/custom_banner');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $custom_banner_id) {
                $this->model_design_custom_banner->deleteCustomBanner($custom_banner_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('design/custom_banner', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getList();
    }

    protected function getList()
    {
        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'name';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('design/custom_banner', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        $data['add'] = $this->url->link('design/custom_banner/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['delete'] = $this->url->link('design/custom_banner/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);

        $data['custom_banners'] = array();

        $filter_data = array(
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $custom_banner_total = $this->model_design_custom_banner->getTotalCustomBanners();

        $results = $this->model_design_custom_banner->getCustomBanners($filter_data);

        foreach ($results as $result) {
            $data['custom_banners'][] = array(
                'banner_id' => $result['banner_id'],
                'type' => $result['type'],
                'category_id' => $result['category_id'],
                'manufacturer_id' => $result['manufacturer_id'],
                'manufacturer' => $result['manufacturer'],
                'name' => $result['name'],
                'description' => $result['description'],
                'status' => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
                'edit' => $this->url->link('design/custom_banner/edit', 'user_token=' . $this->session->data['user_token'] . '&banner_id=' . $result['banner_id'] . $url, true)
            );
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array)$this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }


        $url = '';

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['sort_name'] = $this->url->link('design/custom_banner', 'user_token=' . $this->session->data['user_token'] . '&sort=name' . $url, true);
        $data['sort_status'] = $this->url->link('design/custom_banner', 'user_token=' . $this->session->data['user_token'] . '&sort=status' . $url, true);


        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total = $custom_banner_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('design/custom_banner', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($custom_banner_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($custom_banner_total - $this->config->get('config_limit_admin'))) ? $custom_banner_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $custom_banner_total, ceil($custom_banner_total / $this->config->get('config_limit_admin')));

        $data['sort'] = $sort;
        $data['order'] = $order;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('design/custom_banner_list', $data));
    }

    protected function getForm()
    {
        $data['text_form'] = !isset($this->request->get['banner_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['custom_banner_image'])) {
            $data['error_custom_banner_image'] = $this->error['custom_banner_image'];
        } else {
            $data['error_custom_banner_image'] = array();
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('design/custom_banner', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        if (!isset($this->request->get['banner_id'])) {
            $data['action'] = $this->url->link('design/custom_banner/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        } else {
            $data['action'] = $this->url->link('design/custom_banner/edit', 'user_token=' . $this->session->data['user_token'] . '&banner_id=' . $this->request->get['banner_id'] . $url, true);
        }

        $data['cancel'] = $this->url->link('design/custom_banner', 'user_token=' . $this->session->data['user_token'] . $url, true);

        if (isset($this->request->get['banner_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $custom_banner_info = $this->model_design_custom_banner->getCustomBanner($this->request->get['banner_id']);
        }

        $data['user_token'] = $this->session->data['user_token'];

        if (isset($this->request->post['name'])) {
            $data['name'] = $this->request->post['name'];
        } elseif (!empty($custom_banner_info)) {
            $data['name'] = $custom_banner_info['name'];
        } else {
            $data['name'] = '';
        }
        if (isset($this->request->post['description'])) {
            $data['description'] = $this->request->post['description'];
        } elseif (!empty($custom_banner_info)) {
            $data['description'] = $custom_banner_info['description'];
        } else {
            $data['description'] = '';
        }


        if (isset($this->request->post['type'])) {
            $data['type'] = $this->request->post['type'];
        } elseif (!empty($custom_banner_info)) {
            $data['type'] = $custom_banner_info['type'];
        } else {
            $data['type'] = '';
        }
        if (isset($this->request->post['manufacturer'])) {
            $data['manufacturer'] = $this->request->post['manufacturer'];
        } elseif (!empty($custom_banner_info)) {
            $data['manufacturer'] = $custom_banner_info['manufacturer'];
        } else {
            $data['manufacturer'] = '';
        }

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($custom_banner_info)) {
            $data['status'] = $custom_banner_info['status'];
        } else {
            $data['status'] = true;
        }

        if (isset($this->request->post['category_id'])) {
            $data['category_id'] = $this->request->post['category_id'];
        } elseif (!empty($custom_banner_info)) {
            $data['category_id'] = $custom_banner_info['category_id'];
        } else {
            $data['category_id'] = true;
        }

        if (isset($this->request->post['manufacturer_id'])) {
            $data['manufacturer_id'] = $this->request->post['manufacturer_id'];
        } elseif (!empty($custom_banner_info)) {
            $data['manufacturer_id'] = $custom_banner_info['manufacturer_id'];
        } else {
            $data['manufacturer_id'] = true;
        }

        $this->load->model('localisation/language');

        $data['languages'] = $this->model_localisation_language->getLanguages();

        $this->load->model('tool/image');

        if (isset($this->request->post['custom_banner_image'])) {
            $custom_banner_images = $this->request->post['custom_banner_image'];
        } elseif (isset($this->request->get['banner_id'])) {
            $custom_banner_images = $this->model_design_custom_banner->getCustomBannerImages($this->request->get['banner_id']);
        } else {
            $custom_banner_images = array();
        }

        $data['custom_banner_image'] = array();

        foreach ($custom_banner_images as $key => $value) {
            foreach ($value as $custom_banner_image) {
                if (is_file(DIR_IMAGE . $custom_banner_image['image'])) {
                    $image = $custom_banner_image['image'];
                    $thumb = $custom_banner_image['image'];
                } else {
                    $image = '';
                    $thumb = 'no_image.png';
                }

                if (is_file(DIR_IMAGE . $custom_banner_image['image_m'])) {
                    $image_m = $custom_banner_image['image_m'];
                    $thumb_m = $custom_banner_image['image_m'];
                } else {
                    $image_m = '';
                    $thumb_m = 'no_image.png';
                }

                $data['custom_banner_image'][$key][] = array(
                    'title' => $custom_banner_image['title'],
                    'link' => $custom_banner_image['link'],
                    'image' => $image,
                    'image_m' => $image_m,
                    'thumb' => $this->model_tool_image->resize($thumb, 100, 100),
                    'thumb_m' => $this->model_tool_image->resize($thumb_m, 100, 100),
                    'sort_order' => $custom_banner_image['sort_order']
                );
            }
        }

        $data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('design/custom_banner_form', $data));
    }

    protected function validateForm()
    {
        if (!$this->user->hasPermission('modify', 'design/custom_banner')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }


        if (isset($this->request->post['custom_banner_image'])) {
            foreach ($this->request->post['custom_banner_image'] as $language_id => $value) {
                foreach ($value as $custom_banner_image_id => $custom_banner_image) {
                    if ((utf8_strlen($custom_banner_image['title']) < 2) || (utf8_strlen($custom_banner_image['title']) > 64)) {
                        $this->error['custom_banner_image'][$language_id][$custom_banner_image_id] = $this->language->get('error_title');
                    }
                }
            }
        }

        return !$this->error;
    }

    protected function validateDelete()
    {
        if (!$this->user->hasPermission('modify', 'design/custom_banner')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }
}