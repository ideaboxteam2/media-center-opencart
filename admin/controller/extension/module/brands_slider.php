<?php

class ControllerExtensionModuleBrandsSlider extends Controller
{

    const DEFAULT_MODULE_SETTINGS = [
        'name' => 'Module Name',
        'title' => 'Brands Slider',
        'limit' => 5,
        'status' => 1 /* Enabled by default*/
    ];

    private $error = array();

    public function index()
    {
        if (!isset($this->request->get['module_id'])) {
            $module_id = $this->addModule();
            $this->response->redirect($this->url->link('extension/module/brands_slider', '&user_token=' . $this->session->data['user_token'] . '&module_id=' . $module_id));
        } else {
            $this->editModule($this->request->get['module_id']);
        }
    }

    private function addModule()
    {
        $this->load->model('setting/module');

        $this->model_setting_module->addModule('brands_slider', self::DEFAULT_MODULE_SETTINGS);

        return $this->db->getLastId();
    }

    protected function editModule($module_id)
    {
        $this->load->model('setting/module');

        /* Set page title */
        $this->load->language('extension/module/brands_slider');
        $this->document->setTitle($this->language->get('heading_title'));

        if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) {
            $this->model_setting_module->editModule($this->request->get['module_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
        }
        $data = array();
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_extension'),
            'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/brands_slider', 'user_token=' . $this->session->data['user_token'], true)
        );

        $module_setting = $this->model_setting_module->getModule($module_id);

        if (isset($this->request->post['name'])) {
            $data['name'] = $this->request->post['name'];
        } else {
            $data['name'] = $module_setting['name'];
        }

        if (isset($this->request->post['title'])) {
            $data['title'] = $this->request->post['title'];
        } else {
            $data['title'] = $module_setting['title'];
        }
        if (isset($this->request->post['limit'])) {
            $data['limit'] = $this->request->post['limit'];
        } else {
            $data['limit'] = $module_setting['limit'];
        }

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } else {
            $data['status'] = $module_setting['status'];
        }
        $data['action']['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module');
        $data['action']['save'] = "";
        $data['error'] = $this->error;
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $htmlOutput = $this->load->view('extension/module/brands_slider', $data);
        $this->response->setOutput($htmlOutput);
    }

    public function validate()
    {
        if (!$this->user->hasPermission('modify', 'extension/module/brands_slider')) {
            $this->error['permission'] = true;
            return false;
        }

        if (!utf8_strlen($this->request->post['name'])) {
            $this->error['name'] = true;
        }
        if (!utf8_strlen($this->request->post['title'])) {
            $this->error['title'] = true;
        }
        if (!utf8_strlen($this->request->post['limit'])) {
            $this->error['limit'] = true;
        }

        return empty($this->error);
    }

    public function install()
    {
        $this->load->model('setting/setting');
        $this->model_setting_setting->editSetting('module_brands_slider', ['module_brands_slider_status' => 1]);
    }

    public function uninstall()
    {
        $this->load->model('setting/setting');
        $this->model_setting_setting->deleteSetting('module_brands_slider');
    }
}